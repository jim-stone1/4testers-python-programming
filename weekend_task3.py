import random
from datetime import datetime


# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

GENDER_CHOICES = {'f': female_fnames, 'm': male_fnames}
PERSONS_PER_GENDER = 5
AGE_RANGE = [5,45]

class Person():

    def __init__(self, gender):
        self.firstname = random.choice(GENDER_CHOICES.get(gender))
        self.lastname = random.choice(surnames)
        self.country = random.choice(countries)
        self.age = random.randint(*AGE_RANGE)
    @property
    def email (self):
        return f'{self.firstname}.{self.lastname}@example.com'.lower()

    @property
    def adult(self):
        return True if self.age >= 18 else False

    @property
    def birth_year(self):
        return datetime.today().year - self.age

    def to_dict(self):
        d = self.__dict__.copy()
        d.update({'email': self.email, 'adult': self.adult, 'birth_year': self.birth_year})
        return d

    def __repr__(self):
        return f'Hi! I\'m {self.firstname} {self.lastname}' \
        f' I come from {self.country} and I was born in {self.birth_year}.'


def generate_population(gender_choices: dict, persons_per_gender:int) -> list:
    population = []
    for g in gender_choices:
        for i in range(persons_per_gender):
            person = Person(g)
            population.append(person.to_dict())
            print(person)
    return population


if __name__ == '__main__':
    result = generate_population(GENDER_CHOICES, PERSONS_PER_GENDER)
    print(r
