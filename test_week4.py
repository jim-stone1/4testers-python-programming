import pytest
from week4 import Car, InvalidCarUsageException

@pytest.fixture()
def good_car():
    return Car(model='bmw', color='blue', prod_year=2029)


def test_creates_car_with_all_proper_attributes (good_car):
    assert (good_car.__dict__) == dict(
        model='bmw', color='blue', prod_year=2029, mileage=0
    )

def test_driving_changes_mileage (good_car):
    good_car.drive(4)
    good_car.drive(7)
    assert good_car.mileage == 11

def test_cant_drive_negative_distance(good_car):
    with pytest.raises(InvalidCarUsageException) as excinfo:
        good_car.drive(-0.0001)
    assert str(excinfo.value) == 'Cant decrease mileage'
    assert good_car.mileage == 0

def test_repaint_changes_color(good_car):
    good_car.repaint('black')
    assert good_car.color == 'black'

def test_car_age_properly_calculated(good_car):
    assert good_car.age == -5