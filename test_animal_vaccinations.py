from animal_vaccinations import animal_requires_vaccinations

def test_cat_age_one ():
    assert animal_requires_vaccinations('cat', 1)


def test_dog_age_one ():
    assert animal_requires_vaccinations('dog', 1)