import pytest
import weekend_task

def test_list_of_dicts_has_ten_items():
    lod = weekend_task.construct_person_dicts(
        weekend_task.draw_firstnames_list(weekend_task.female_fnames, weekend_task.male_fnames),
        weekend_task.surnames,
        weekend_task.countries)
    assert len(lod) == 10

def test_list_of_dicts_has_five_female():
    lod = weekend_task.construct_person_dicts(
        weekend_task.draw_firstnames_list(weekend_task.female_fnames, weekend_task.male_fnames),
        weekend_task.surnames,
        weekend_task.countries)
    lod_females = [d for d in lod if d['firstname'] in weekend_task.female_fnames]
    assert len(lod_females) == 5


def test_list_of_dicts_has_five_male():
    lod = weekend_task.construct_person_dicts(
        weekend_task.draw_firstnames_list(weekend_task.female_fnames, weekend_task.male_fnames),
        weekend_task.surnames,
        weekend_task.countries)
    lod_males = [d for d in lod if d['firstname'] in weekend_task.male_fnames]
    assert len(lod_males) == 5