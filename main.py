import math
import random
from typing import Sequence

def print_bio(**kwargs):
    for k in kwargs:
        print (f'{k}: {kwargs.get(k)}')


def calculate_average(numbers: Sequence):
    condition1 = len(numbers) > 0
    condition2 = all([type (n) is (int or float) for n in numbers])
    assert condition1 and condition2, 'Invalid input. Please pass sequence of numbers'
    return sum(numbers)/len(numbers)


def check_divisors(start, end, divisor):
    for i in range (start, end):
        if i % divisor == 0:
            print (i)

def return_ten():
    return random.sample(range(1000, 5001), 10)

if __name__ == '__main__':
    # calculate_average([1, 2])
    # print (1 == 1.0)
    # check_divisors(0,31, 7)
    print(return_ten())





















    # print_bio(name='kuba', pets=2)
    # print (True + True + True)
    #
    # list_input = [2, 34, 234234, -4, 2445]
    # str_input = 'kuba'
    # print (sorted(list_input)[-3:])
    #
    # def return_max_three(mylist):
    #     return sorted(mylist)[-3:]
    #
    # res = return_max_three(list_input)
    # print (res)
    #
    # d = dict (name='lula', gatunek='kot')
    # print (d.items())