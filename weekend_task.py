import random
from datetime import datetime

PERSONS_NUMBER = 10

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

def draw_firstnames_list(male_names, female_names):
    result = []
    for i in range (int(PERSONS_NUMBER/2)):
        result.append(random.choice(male_names))
        result.append(random.choice(female_names))
    return result
def construct_person_dicts(fnames, surnames, countries, quantity=PERSONS_NUMBER):
    results = []
    for i in range(quantity):
        firstname = fnames [i]
        lastname = random.choice(surnames)
        age = random.randint(5,45)
        d = {
            'firstname': firstname,
            'lastname': lastname,
            'country': random.choice(countries),
            'email': f'{firstname.lower()}.{lastname.lower()}@example.com',
            'age': age,
            'adult': True if age >= 18 else False,
            'birth_year': datetime.today().year - age
            }
        results.append(d)
    return results

def create_person_representation(person_dict):
    return 'Hi! I\'m {0} {1}. I come from {2} and I was born in {3}'.format(
        person_dict['firstname'].upper(), person_dict['lastname'].upper(),
        person_dict['country'].upper(), person_dict['birth_year'])

if __name__  == '__main__':
    fnames = draw_firstnames_list(female_fnames, male_fnames)
    persons = construct_person_dicts(fnames, surnames, countries)
    for p in persons:
        repr = create_person_representation(p)
        print(repr)
