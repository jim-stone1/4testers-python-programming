from datetime import datetime
from faker import Faker
from faker.providers import automotive, date_time, DynamicProvider

class InvalidCarUsageException(Exception):
    def __init__(self, message):
        super().__init__(message)

class Car():
    def __init__(self, model, color, prod_year):
        self.model = model
        self.color = color
        self.prod_year = prod_year
        self.mileage = 0

    def drive(self, distance):
        self._validate_distance(distance)
        self.mileage += distance

    @property
    def age(self):
        return datetime.now().year - self.prod_year

    def repaint(self, color):
        self.color = color

    def __repr__(self):
        return f'{self.color.capitalize()} {self.model.capitalize()} {self.prod_year}'

    @staticmethod
    def _validate_distance(distance):
        if distance < 0:
            raise InvalidCarUsageException('Cant decrease mileage')

if __name__ == '__main__':
    car_models_provider = DynamicProvider(
        provider_name='car_model',
        elements='toyota bmw skoda porsche fiat ford iveco mercedes trabant'.split(' ')
    )
    fake = Faker()
    fake.add_provider(automotive)
    fake.add_provider(date_time)
    fake.add_provider(car_models_provider)
    cars = []
    for i in range(3):
        cars.append(
            Car(model=fake.car_model(),
                 color=fake.color_name(),
                 prod_year=fake.date_time_this_century().year
            )
        )
    print(cars)
