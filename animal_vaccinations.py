
def animal_requires_vaccinations(species=None, age=None):
    # wartośći domyślne przenoszą wyłapywanie błędu braku argumentów
    # z wywołania funkcji do uruchomienia walidacji
    vaccination_periods = {'cat': 3, 'dog': 2}
    animal_species_validation = species in vaccination_periods.keys()
    animal_age_validation = type(age) is int and age > 0
    if not (animal_species_validation and animal_age_validation):
        return TypeError('Error! Invalid input. Allowed species: cat, dog. Allowed age: positive integer.')
    # powód użycia return zamiast raise:
    # ułatwia skonstruowanie funkcji generującej wynik testów
    return (age - 1) % vaccination_periods.get(species) == 0


def test_animal_requires_vaccination():
    test_cases = [
        ['cat', 4, True],
        ['cat', '4', TypeError],
        ['dog', 1, True],
        ['cat', 0, TypeError],
        ['dog', 11.5, TypeError],
        ['cat', 8.5, TypeError],
        ['dog', 11, True],
        ['dog', 12, False],
        ['dog', 13, True],
        ['cat', 13, True],
        ['cat', 14, False],
        ['cat', 15, False],
        ['cat', 16, True],
        ['cat', '1', TypeError],
        ['barnaba', 'sto', TypeError],
        [None, 4, TypeError],
        ['cat', None, TypeError]
    ]
    test_results = []
    for tc in test_cases:
        function_result = animal_requires_vaccinations(tc[0], tc[1])
        if tc[2] is TypeError:
            tc_result = 'pass' if type(function_result) == TypeError else 'fail'
        else:
            tc_result = 'pass' if function_result == tc[2] else 'fail'
        tc.append(tc_result)
        if tc_result == 'fail':
            tc.append(f'{str(function_result).upper()} does not equal {str(tc[2]).upper()}')
        test_results.append(tc)
    return test_results


if __name__ == '__main__':
    results = test_animal_requires_vaccination()
    for r in results:
        print(r)
